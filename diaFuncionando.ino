/* Projeto 01 - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 * Guilherme Rosa - RA 157955 
 */

//bibliotecas
#include "TimerOne.h"

//defines
#define base 1000 //base de 1ms
#define carR 12
#define carY 11
#define carG 10
#define pedR 9
#define pedG 8
#define button 2
#define LDRSensor 0 

 
//funções
void iniciaPinos();
void increaseTime(); // ISR que é responsável pela sincronização dos eventos no Loop
void interruptOn();
void interruptOff();
void allowButtonISR();
void estado_1(); //  aberto para carros e fechado pedestres
void estado_2(); // amarelo para carros  e fechado para pedestres
void estado_3(); // fechado para carros e aberto para pedestres
void estado_4(bool pisca);
void estado_noturno(bool pisca);

//variaveis globais
int timeCounter=0; //variavel contadora do tempo, segundo a formula tempo = timeCounter * base
int sequency = 0;
int timeButton = 0;
bool allowButton = false;
bool seguranca = false; //variavel para garantir que só a primeira vez que o botao for pressionado será levado em conta
bool isDay = false;
int counterPiscada = 0;

//programa
void setup() {
  Serial.begin(9600);
  interruptOn();
  iniciaPinos();
  estado_1();//chama o estado inicial  
}

void loop() {
    
  if((digitalRead(button)== LOW) && (seguranca == false)){
    seguranca = true;
    timeCounter = 0;
    }
    
  if((timeCounter >= 3000) && (sequency == 0) && (seguranca==true)){
    timeCounter = 0;
    estado_2();//amarelo carro
    sequency = 1;
   }

if((timeCounter >= 3000) && (sequency == 1) && (seguranca)){
    timeCounter = 0;
    estado_3();//verde pedestre
    sequency = 2;
   }

if ((timeCounter >= 10000) && (sequency == 2) && (seguranca)){
    timeCounter = 0;
    counterPiscada = 0;

    while(counterPiscada<10000){
      if (timeCounter>=500 && sequency == 2){
        estado_4(false);
        timeCounter = 0;
        sequency = 3;
      }

      if (timeCounter>=500 && sequency == 3){
       estado_4(true);
       timeCounter = 0;
       sequency = 2;
      }
     }

    seguranca = false;
    estado_1();
    sequency = 0;
}

}

void estado_1(){
  digitalWrite(carG,HIGH);
  digitalWrite(carY,LOW);
  digitalWrite(carR,LOW);
  digitalWrite(pedG,LOW);
  digitalWrite(pedR,HIGH);
  }

void estado_2(){
  digitalWrite(carG,LOW);
  digitalWrite(carY,HIGH);
  digitalWrite(carR,LOW);
  digitalWrite(pedG,LOW);
  digitalWrite(pedR,HIGH);
 }

void estado_3(){
  digitalWrite(carG,LOW);
  digitalWrite(carY,LOW);
  digitalWrite(carR,HIGH);
  digitalWrite(pedG,HIGH);
  digitalWrite(pedR,LOW);
 }

void estado_4(bool pisca){
  digitalWrite(carG,LOW);
  digitalWrite(carY,LOW);
  digitalWrite(carR,HIGH);
  if (pisca){
    digitalWrite(pedG,LOW);
    digitalWrite(pedR,HIGH);  
  }
  else {
    digitalWrite(pedG,LOW);
    digitalWrite(pedR,LOW);
   }
 }
void estado_noturno(bool pisca);

void iniciaPinos(){
  pinMode(carG,OUTPUT);
  pinMode(carY,OUTPUT);
  pinMode(carR,OUTPUT);
  pinMode(pedR,OUTPUT);
  pinMode(pedG,OUTPUT);
  pinMode(button,INPUT);
  }


void increaseTime(){
 timeCounter++;
 counterPiscada++;
 }

void interruptOn(){
  Timer1.initialize(base);
  Timer1.attachInterrupt(increaseTime);
  }
  
void interruptOff(){
    Timer1.detachInterrupt(); 
  }

  