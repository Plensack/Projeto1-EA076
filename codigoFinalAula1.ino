/* Projeto 01 - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 * Guilherme Rosa - RA 157955 
 */

//bibliotecas
#include "TimerOne.h"

//defines
#define base 1000 //base de 1ms
#define carR 12
#define carY 11
#define carG 10
#define pedR 9
#define pedG 8
/* 
 * os defines abaixo descrevem os estados possíveis para os farois 
 * Com a convenção 1-> ACESO e 0->APAGADO
 * 
 * A correspondência entre os bits e as luzes é:
 * 
 * Carro Vermelho/Carro Amarelo/Carro Verde/Pedestre Vermelho/Pedestre Verde
 */
 
//funções
void iniciaPinos();
void increaseTime(); // ISR que é responsável pela sincronização dos eventos no Loop
void interruptOn();
void interruptOff();
void allowButtonISR();

//variaveis globais
int timeCounter=0; //variavel contadora do tempo, segundo a formula tempo = timeCounter * base
int sequency = 0;
int timeButton = 0;
bool allowButton = false;

//programa
void setup() {
  interruptOn();
  Serial.begin(9600);
  pinMode(pedR,OUTPUT);
  pinMode(13,OUTPUT);
  digitalWrite(13,LOW);
  attachInterrupt(0,allowButtonISR,RISING); 
}

void loop() {

  if((timeCounter >= 1000) && (sequency == 0)){
    timeCounter = 0;
    digitalWrite(pedR,HIGH);
    sequency = 1;
   }

if((timeCounter >= 1000) && (sequency == 1)){
    //interruptOff();
    timeCounter = 0;
    digitalWrite(pedR,LOW);
    sequency = 0;
   }

}

void iniciaPinos(){
  pinMode)
  
  }


void increaseTime(){
 timeCounter++;

 }

void interruptOn(){
  Timer1.initialize(base);
  Timer1.attachInterrupt(increaseTime);
  }
  
void interruptOff(){
    Timer1.detachInterrupt(); 
  }

void allowButtonISR(){
   // allowButton = true;
      digitalWrite(13,HIGH);
  }
  