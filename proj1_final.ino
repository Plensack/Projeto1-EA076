/* Projeto 01 - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 *  Guilherme Rosa - RA 157955 
 */

//bibliotecas
#include "TimerOne.h" // biblioteca que simplifica a configuração do timer

//defines
#define base 1000 //base de 1ms
#define carR 12 //Farol vermelho carros -> pino digital 12
#define carY 11 //Farol amarelo carros -> pino digital 11
#define carG 10 //Farol verde carros -> pino digital 10
#define pedR 9 // Farol vermelho pedestres -> pino digital 9
#define pedG 8 // Farol verde pedestres -> pino digital 8
#define button 2 // Botao para troca de estados -> pino digital 2
#define LDRSensor 5 // Entrada do ADC do arduino -> pino anologico 5
#define limIndex 20 //Quantidade máxima de dados para a media do LDR
#define debouncingTime 50 // Tempo de debouncing de 50ms
 
//funções
void iniciaPinos(); // faz toda a sequência de definições de pinos como entradas e saidas
void increaseTime(); // ISR que é responsável pela sincronização dos eventos no Loop
void interruptOn(); //ativa a interrução do timer 1
void interruptOff(); // desativa a interrupçaõ do timer 1
void estado_1(); //  aberto para carros e fechado pedestres (dia)
void estado_2(); // amarelo para carros  e fechado para pedestres (dia)
void estado_3(); // fechado para carros e aberto para pedestres (dia)
void estado_4(bool pisca); //fechado para carros e vermelho piscante pedestres (dia)
void estado_noturno(bool pisca); //amarelo piscante carros e vermelho piscante pedestres

//variaveis globais
int timeCounter=0; //variavel contadora do tempo, segundo a formula tempo = timeCounter * base
int sequency = 0; //variavel que armazena o estado atual do sistema durante o dia
int timeButton = 0; // variavel que armazena o tempo desde que o botao foi pressionado
int buttonSequency = 0; // variavel para dar robustez ao debouncing do botao, criando dois estados para o mesmo  
bool allowButton = false; // variavel que faz o debouncing por software
bool seguranca = false; //variavel para garantir que só a primeira vez que o botao for pressionado será levado em conta
bool isDay = true; // armazena o estado: se true ->dia ; se false -> noite
int counterLDR = 0; // variavel que temporiza o acontecimento da aquisição dos valores do LDR (0,5s)
int counterPiscada = 0; // temporizador da piscada
int index = 0; //indice do vetor de medidas do LDR
int LDRMeasure[limIndex] = {0}; // vetor de medidas do LDR
int luminosity = 0; // media dos valores de luminosidade
int nightSequency = 0; //variavel que armazena o estado atual do sistema durante a noite

//programa
void setup() {
  interruptOn(); // ativa a interrupção por tempo a cada 1ms
  iniciaPinos(); // inicia os pinos 
  estado_1();//chama o estado inicial durante o dia
}

void loop() {

  if (counterLDR>=500){ //0,5s para cada medição do sinal do LDR
    LDRMeasure[index] = analogRead(LDRSensor);//aquisição da medida
    index++;
    counterLDR = 0; // reinicia tempo para proxima medida
    if(index >= limIndex){ //trata o fim das medidas
      index = 0;
      for (int i = 0; i < limIndex;i++){//soma os valores e zera o vetor
       luminosity += LDRMeasure[i]; 
       LDRMeasure[i] = 0;  
      }
      luminosity = luminosity/limIndex;//tira a media
      
      if (luminosity>=350){ // decide entre dia e noite. 
      /*
      Por experimentação, chegamos a concluão que o LDR variava de 90k(escuro) à 10k(luz da sala), por isso o resistor escolhido foi de 10k. 
      Sendo assim, temos que o valor de tensão na medida no LDR iria estar na faixa de 200 para o escuro e de 500 para a luz da sala.
      Dessa forma, a  escolha de 350 é para garantir um certa margem de segurança para o valor da média. 
      */
        isDay = true;//dia
      } else {
        isDay = false;//noite
      }
     }
   }


   if (!isDay && sequency == 0 && nightSequency == 0 && timeCounter>=500){//estado 1 da noite - parte 1
     timeCounter = 0;//zera o timer
     estado_noturno(false);//aplica estado - luz piscante apagada
     nightSequency = 1; // estado seguinte
    }

   if (!isDay && sequency == 0 && nightSequency == 1 && timeCounter>=500){//estado 1 da noite - parte 2
     timeCounter = 0;//zera o timer
     estado_noturno(true);//aplica estado - luz piscante acesa
     nightSequency = 0;// estado seguinte
   }


   if (isDay && sequency == 0){//aplica o estado 1 do dia
     estado_1();//aplica o estado
   }

  if(isDay && (digitalRead(button)== LOW) && (seguranca == false) && buttonSequency == 0){//captura o inicio do aperto do botao
    timeButton = 0;//zera timer do botao
    buttonSequency = 1;
    }

  if(!seguranca && isDay && (digitalRead(button)== LOW) && (timeButton >= debouncingTime) && buttonSequency == 1){//autoriza a reação do sistema a leitura do botao
    seguranca = true;//permite a mudança de estado do farol
    buttonSequency = 0;//retorna ao estado anterior
    timeCounter = 0;
    }


  if(isDay && (timeButton >= 500) && buttonSequency == 1){
    buttonSequency = 0;//trata o caso de falha, onde o botao demorou muito para estabilizar
    }

    
  if((timeCounter >= 3000) && (sequency == 0) && (seguranca==true)){//estado 2 - 3s apos o botao validado
    timeCounter = 0; // zera tempo
    estado_2();//amarelo carro
    sequency = 1;//permite seguinte
   }

  if((timeCounter >= 3000) && (sequency == 1) && (seguranca)){//estado 3 - 3s apos estado 2
      timeCounter = 0;//zera tempo
      estado_3();//verde pedestre
      sequency = 2; // permite o seguinte
     }

  if ((timeCounter >= 5000) && (sequency == 2) && (seguranca)){//estado 4 - 5s apos estado 3
      timeCounter = 0;//zera timer
      counterPiscada = 0;//zera timer para piscar
      sequency = 3;//permite o estagio 4 com luz apagada
      }

  if (timeCounter>=500 && sequency == 3){//estado 4 com luz apagada
      estado_4(false);//
      sequency = 4;//
      timeCounter = 0;//zera o timer
      }

  if (timeCounter>=500 && sequency == 4){//estado 4 com luz acesa
      sequency = 3;
      estado_4(true);
      timeCounter = 0;
      }

  if(counterPiscada>5000 && sequency>=3){//retorno do estado 3 ou 4 para o estado 1 - apos 5s piscando
    seguranca = false;
    estado_1();
    sequency = 0; 
    }

 }


void estado_1(){
  digitalWrite(carG,HIGH);
  digitalWrite(carY,LOW);
  digitalWrite(carR,LOW);
  digitalWrite(pedG,LOW);
  digitalWrite(pedR,HIGH);
  }

void estado_2(){
  digitalWrite(carG,LOW);
  digitalWrite(carY,HIGH);
  digitalWrite(carR,LOW);
  digitalWrite(pedG,LOW);
  digitalWrite(pedR,HIGH);
 }

void estado_3(){
  digitalWrite(carG,LOW);
  digitalWrite(carY,LOW);
  digitalWrite(carR,HIGH);
  digitalWrite(pedG,HIGH);
  digitalWrite(pedR,LOW);
 }

void estado_4(bool pisca){
  digitalWrite(carG,LOW);
  digitalWrite(carY,LOW);
  digitalWrite(carR,HIGH);
  if (pisca){
    digitalWrite(pedG,LOW);
    digitalWrite(pedR,HIGH);  
  }
  else {
    digitalWrite(pedG,LOW);
    digitalWrite(pedR,LOW);
   }
 }

void estado_noturno(bool pisca){
  if (pisca){
    digitalWrite(carG,LOW);
    digitalWrite(carY,HIGH);
    digitalWrite(carR,LOW);
    digitalWrite(pedG,LOW);
    digitalWrite(pedR,HIGH);
  
  } else {
    digitalWrite(carG,LOW);
    digitalWrite(carY,LOW);
    digitalWrite(carR,LOW);
    digitalWrite(pedG,LOW);
    digitalWrite(pedR,LOW);
   
  }

  }

void iniciaPinos(){
  pinMode(carG,OUTPUT);
  pinMode(carY,OUTPUT);
  pinMode(carR,OUTPUT);
  pinMode(pedR,OUTPUT);
  pinMode(pedG,OUTPUT);
  pinMode(button,INPUT);
  }


void increaseTime(){//funcao de interrupcao onde somamos 4 variaveis e implementamos 4 sub-timers
 timeCounter++;
 counterPiscada++;
 counterLDR++;
 timeButton++;
 }

void interruptOn(){//liga as interrupcoes do timerOne
  Timer1.initialize(base);
  Timer1.attachInterrupt(increaseTime);
  }
  
void interruptOff(){//desliga as interrupcoes do timerOne
    Timer1.detachInterrupt(); 
  }
